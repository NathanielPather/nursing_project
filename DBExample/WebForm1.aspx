﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="DBExample.WebForm1" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <link rel="stylesheet" type="text/css" href="StyleSheet1.css"/>
        <title></title>
    </head>
    <body style="height: 569px">
        <!-- Whole page container -->
        <div id="container1">
            <form id="form1" runat="server">
                <!-- Whole form container -->
                <div id="container2">
                    <!-- left form container -->
                    <div id="container3">
                        <!-- Data entry text boxes -->
                        <div>
                            <asp:Label ID="NameLabel" runat="server">Organisation Name</asp:Label>
                        </div>
                        <div>
                            <asp:TextBox ID="NameTextBox" runat="server" CssClass="textfield" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
                        </div>
                        <div>
                            <asp:Label ID="SettingLabel" runat="server">Care Setting</asp:Label>
                        </div>
                        <div>
                            <asp:DropDownList ID="CareSettingDropDown" runat="server" CssClass="textfield" >
                                    <asp:ListItem Text="Residential Care" Value="residential"></asp:ListItem>
                                    <asp:ListItem Text ="Community Care" Value="community"></asp:ListItem>
                                    <asp:ListItem Text="Hospital" Value="hospital"></asp:ListItem>
                                    <asp:ListItem Text ="Hospice" Value="hospice"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div>
                            <asp:Label ID="StartLabel" runat="server">Start Time</asp:Label>
                        </div>
                        <div>
                            <asp:TextBox ID="StartTextBox" runat="server" CssClass="textfield" TextMode="Time"></asp:TextBox>
                        </div>
                        <div>
                            <asp:Label ID="EntryLabel" runat="server">Entry Instructions</asp:Label>
                        </div>
                        <div>
                            <asp:TextBox ID="EntryTextBox" CssClass="textfield" runat="server" TextMode="Multiline" Rows="8" />
                        </div>
                    </div>
                    <!-- right form container -->
                    <div id="container4">
                         <div>
                            <asp:Label ID="SupervisorLabel" runat="server">Supervisor Name</asp:Label>
                        </div>
                        <div>
                            <asp:TextBox ID="SupervisorTextBox" CssClass="textfield" runat="server"></asp:TextBox>
                        </div>
                        <div>
                            <asp:Label ID="DesignationLabel" runat="server">Staff Designation</asp:Label>
                        </div>
                        <div>
                            <asp:DropDownList ID="StaffDesignationDropDown" runat="server" CssClass="textfield">
                                <asp:ListItem Text="Nursing Assistant/Care Worker" Value="Nursing Assistant/Care Worker"></asp:ListItem>
                                <asp:ListItem Text ="Enrolled Nurse" Value="Enrolled Nurse"></asp:ListItem>
                                <asp:ListItem Text="Endorsed Enrolled Nurse" Value="Endorsed Enrolled Nurse"></asp:ListItem>
                                <asp:ListItem Text ="Registered Nurse" Value="Registered Nurse"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div>
                            <asp:Label ID="EndLabel" runat="server">End Time</asp:Label>
                        </div>
                        <div>
                            <asp:TextBox ID="EndTextBox" runat="server" CssClass="textfield" TextMode="Time"></asp:TextBox>
                        </div>
                        <div>
                            <asp:Label ID="SpecialLabel" runat="server">Special Request</asp:Label>
                        </div>
                        <div>
                            <asp:TextBox ID="SpecialTextBox" CssClass="textfield" runat="server" TextMode="Multiline" Rows="8" />
                        </div>
                    </div>
                </div>
                <div id="table" runat="server">
                <!-- SQL Data Source -->
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Table]"></asp:SqlDataSource>
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Submit" />
                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                        <asp:BoundField DataField="OrganisationName" HeaderText="OrganisationName" SortExpression="OrganisationName" />
                        <asp:BoundField DataField="CareSetting" HeaderText="CareSetting" SortExpression="CareSetting" />
                        <asp:BoundField DataField="StaffDesignation" HeaderText="StaffDesignation" SortExpression="StaffDesignation" />
                        <asp:BoundField DataField="StartTime" HeaderText="StartTime" SortExpression="StartTime" />
                        <asp:BoundField DataField="EndTime" HeaderText="EndTime" SortExpression="EndTime" />
                        <asp:BoundField DataField="SupervisorName" HeaderText="SupervisorName" SortExpression="SupervisorName" />
                        <asp:BoundField DataField="EntryInstructions" HeaderText="EntryInstructions" SortExpression="EntryInstructions" />
                        <asp:BoundField DataField="SpecialRequest" HeaderText="SpecialRequest" SortExpression="SpecialRequest" />
                    </Columns>
                    </asp:GridView>
                </div>
            </form>
        </div>
    </body>
</html>