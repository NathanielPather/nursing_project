﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace DBExample {
    public partial class WebForm1 : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            if (IsPostBack) {
                // Set connection string.
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                // Open database connection with specified string.
                conn.Open();
                // Checks if organisation name in textbox exists in the database.
                // Should be removed.
                string checkuser = "select count(*) from [Table] where organisationName='" + NameTextBox.Text + "'";
                // Run the command for the given database.
                SqlCommand cmd = new SqlCommand(checkuser, conn);
                // Temporary variable for the count of the organisation name in the textbox, checked against the database.
                // Should be removed.
                int temp = Convert.ToInt32(cmd.ExecuteScalar().ToString());

                if (temp == 1)
                {
                    Response.Write("Student Already Exist");
                }

                conn.Close();
            }
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                conn.Open();
                string insertQuery = "insert into [Table](OrganisationName,CareSetting,StaffDesignation,StartTime,EndTime,SupervisorName,EntryInstructions,SpecialRequest)values (@organisationname,@caresetting,@staffdesignation,@starttime,@endtime,@supervisorname,@entryinstructions,@specialrequest)";
                SqlCommand cmd = new SqlCommand(insertQuery, conn);
                cmd.Parameters.AddWithValue("@organisationname", NameTextBox.Text);
                cmd.Parameters.AddWithValue("@caresetting", CareSettingDropDown.SelectedValue);
                cmd.Parameters.AddWithValue("@staffdesignation", StaffDesignationDropDown.SelectedValue);
                cmd.Parameters.AddWithValue("@starttime", StartTextBox.Text);
                cmd.Parameters.AddWithValue("@endtime", EndTextBox.Text);
                cmd.Parameters.AddWithValue("@supervisorname", SupervisorTextBox.Text);
                cmd.Parameters.AddWithValue("@entryinstructions", EntryTextBox.Text);
                cmd.Parameters.AddWithValue("@specialrequest", SpecialTextBox.Text);
                cmd.ExecuteNonQuery();

                Response.Write("Person registeration Successfully!!!thank you");
                conn.Close();

            }
            catch (Exception ex)
            {
                Response.Write("error" + ex.ToString());
            }
        }

        protected void TextBox1_TextChanged1(object sender, EventArgs e)
        {

        }
    }
}